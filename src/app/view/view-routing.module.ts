import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GerenciamentoComponent } from 'src/app/view/gerenciamento/gerenciamento.component';
import { MonitoramentoComponent } from 'src/app/view/monitoramento/monitoramento.component';
import { PortariasComponent } from 'src/app/view/portarias/portarias.component';
import { RemocaoComponent } from 'src/app/view/remocao/remocao.component';

const routes: Routes = [
  { path: 'Gerenciamento', component: GerenciamentoComponent },
  { path: 'Monitoramento', component: MonitoramentoComponent },
  { path: 'Portarias', component: PortariasComponent },
  { path: 'Remocao', component: RemocaoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewRoutingModule { }
