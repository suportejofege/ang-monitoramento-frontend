import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-monitoramento',
  templateUrl: './monitoramento.component.html',
  styleUrls: ['./monitoramento.component.css']
})
export class MonitoramentoComponent implements OnInit {

  public tags = [
    { "label":"Argamassa", "value":"" },
    { "label":"Concreto", "value":"" },
    { "label":"Caçamba", "value":"" },
    { "label":"Bitrem", "value":"" },
    { "label":"Diretoria", "value":"" },
    { "label":"Diversos", "value":"" },
    { "label":"Terceiros", "value":"" },
    { "label":"Veículos", "value":"" }
  ]

  constructor() { }

  ngOnInit(): void {
  }


}
